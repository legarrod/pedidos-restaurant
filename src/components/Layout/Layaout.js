import React from 'react';
import Navbar from '../Navbar/Navbar';

export default function Layaout({ child }) {
  return (
    <div className="m-0 p-0">
      <Navbar />
      {child}
    </div>
  );
}
