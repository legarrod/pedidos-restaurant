import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Employees from './pages/Employees';
import Home from './pages/Home';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/employees">
          <Employees />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
